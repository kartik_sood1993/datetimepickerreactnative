import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Button,
  StyleSheet,
  Platform,
} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

const DatePickerModalComponent = props => {
  const [date, setDate] = useState(new Date()); //current date or selected date
  const [show, setShow] = useState(false); // isvisible or not
  const [text, setText] = useState(''); //show date and time
  const [mode, setMode] = useState('date'); //mode ->  date or time or datetime

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setDate(currentDate);
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const hideDatePicker = () => {
    setShow(false);
  };

  const handleConfirm = selectedDate => {
    const currentDate = selectedDate || date;

    setDate(currentDate);

    let tempDate = new Date(currentDate);
    let fDate =
      tempDate.getDate() +
      '/' +
      (tempDate.getMonth() + 1) +
      '/' +
      tempDate.getFullYear();

    let fTime =
      'Hours: ' + tempDate.getHours() + ' Minutes: ' + tempDate.getMinutes();

    setText(fDate + '\n' + fTime);
    hideDatePicker();
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <Text style={{fontWeight: 'bold', fontSize: 18}}>{text}</Text>
        <View style={{marginTop: 20}}>
          <Button title="Date Picker" onPress={() => showMode('date')} />
        </View>
        <View style={{marginTop: 10}}>
          <Button title="Time Picker" onPress={() => showMode('time')} />
        </View>

        <DateTimePickerModal
          isVisible={show}
          mode={mode}
          date={date}
          onChange={onChange}
          onConfirm={handleConfirm}
          onCancel={hideDatePicker}
          is24Hour={true}
          maximumDate={new Date(2021, 10, 15)}
          minimumDate={new Date(2021, 6, 15)}
          display={Platform.OS === 'ios' ? 'spinner' : 'default'}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default DatePickerModalComponent;
