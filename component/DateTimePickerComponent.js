import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Button,
  StyleSheet,
  Platform,
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';

const DateTimePickerComponent = (props) => {
  const [date, setDate] = useState(new Date()); //current date or selected date
  const [show, setShow] = useState(false); // isvisible or not
  const [text, setText] = useState(''); //show date and time
  const [mode, setMode] = useState('date'); //mode ->  date or time or datetime
  
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);

    let tempDate = new Date(currentDate);
    let fDate =
      tempDate.getDate() +
      '/' +
      (tempDate.getMonth() + 1) +
      '/' +
      tempDate.getFullYear();

    let fTime =
      'Hours: ' + tempDate.getHours() + ' Minutes: ' + tempDate.getMinutes();

    setText(fDate + '\n' + fTime);
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <Text style={{fontWeight: 'bold', fontSize: 18}}>{text}</Text>
        <View style={{marginTop: 20}}>
          <Button title="Date Picker" onPress={() => showMode('date')} />
        </View>
        <View style={{marginTop: 10}}>
          <Button title="Time Picker" onPress={() => showMode('time')} />
        </View>
        
        {show && (
          <DateTimePicker
            value={date}
            mode={mode}
            onChange={onChange}
            is24Hour={false}
            display={Platform.OS === 'ios' ? 'spinner' : 'spinner'}
            maximumDate={new Date(2021, 10, 15)}
            minimumDate={new Date(2021, 6, 15)}
            style={styles.datePicker}
          />
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  // this will work only in ios
  datePicker: {
    width:"80%",
    height: 290,
    justifyContent: 'center',

  },
});

export default DateTimePickerComponent;
