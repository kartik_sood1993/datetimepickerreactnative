import React from 'react';
import DatePickerModalComponent from './component/DatePickerModalComponent';
import DateTimePickerComponent from './component/DateTimePickerComponent';

const App = () => {
  return <DatePickerModalComponent />;
};

export default App;
